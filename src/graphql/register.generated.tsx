import * as Types from '../generated/graphql';

import { gql } from '@apollo/client';
import * as Apollo from '@apollo/client';
export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
const defaultOptions = {} as const;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
};

export type Collection = {
  __typename?: 'Collection';
  id: Scalars['Int'];
  name: Scalars['String'];
  tasks: Array<Task>;
};

export type LoginResponse = {
  __typename?: 'LoginResponse';
  accessToken?: Maybe<Scalars['String']>;
  ok: Scalars['Boolean'];
};

export type Mutation = {
  __typename?: 'Mutation';
  createCollection: Collection;
  createTag: Tag;
  createTask: Task;
  login: LoginResponse;
  logout: OkResponse;
  register: LoginResponse;
  updateTaskCompletion: UpdateTaskCompletionResponse;
  updateTaskName: UpdateTaskNameResponse;
};


export type MutationCreateCollectionArgs = {
  name: Scalars['String'];
};


export type MutationCreateTagArgs = {
  name: Scalars['String'];
  taskId: Scalars['Int'];
};


export type MutationCreateTaskArgs = {
  collectionId: Scalars['Int'];
  name: Scalars['String'];
};


export type MutationLoginArgs = {
  name: Scalars['String'];
  password: Scalars['String'];
};


export type MutationRegisterArgs = {
  name: Scalars['String'];
  password: Scalars['String'];
};


export type MutationUpdateTaskCompletionArgs = {
  completed: Scalars['Boolean'];
  taskId: Scalars['Int'];
};


export type MutationUpdateTaskNameArgs = {
  taskId: Scalars['Int'];
  taskName: Scalars['String'];
};

export type OkResponse = {
  __typename?: 'OkResponse';
  ok: Scalars['Boolean'];
};

export type Query = {
  __typename?: 'Query';
  collection?: Maybe<Collection>;
  collections: Array<Collection>;
  task?: Maybe<Task>;
};


export type QueryCollectionArgs = {
  id: Scalars['Int'];
};


export type QueryTaskArgs = {
  id: Scalars['Int'];
};

export type Tag = {
  __typename?: 'Tag';
  id: Scalars['Int'];
  name: Scalars['String'];
};

export type Task = {
  __typename?: 'Task';
  completed: Scalars['Boolean'];
  id: Scalars['Int'];
  name: Scalars['String'];
  tags: Array<Tag>;
};

export type UpdateTaskCompletionResponse = {
  __typename?: 'UpdateTaskCompletionResponse';
  completed: Scalars['Boolean'];
};

export type UpdateTaskNameResponse = {
  __typename?: 'UpdateTaskNameResponse';
  name: Scalars['String'];
};

export type RegisterMutationVariables = Types.Exact<{
  name: Types.Scalars['String'];
  password: Types.Scalars['String'];
}>;


export type RegisterMutation = { __typename?: 'Mutation', register: { __typename?: 'LoginResponse', accessToken?: string | null, ok: boolean } };


export const RegisterDocument = gql`
    mutation Register($name: String!, $password: String!) {
  register(name: $name, password: $password) {
    accessToken
    ok
  }
}
    `;
export type RegisterMutationFn = Apollo.MutationFunction<RegisterMutation, RegisterMutationVariables>;

/**
 * __useRegisterMutation__
 *
 * To run a mutation, you first call `useRegisterMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useRegisterMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [registerMutation, { data, loading, error }] = useRegisterMutation({
 *   variables: {
 *      name: // value for 'name'
 *      password: // value for 'password'
 *   },
 * });
 */
export function useRegisterMutation(baseOptions?: Apollo.MutationHookOptions<RegisterMutation, RegisterMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<RegisterMutation, RegisterMutationVariables>(RegisterDocument, options);
      }
export type RegisterMutationHookResult = ReturnType<typeof useRegisterMutation>;
export type RegisterMutationResult = Apollo.MutationResult<RegisterMutation>;
export type RegisterMutationOptions = Apollo.BaseMutationOptions<RegisterMutation, RegisterMutationVariables>;