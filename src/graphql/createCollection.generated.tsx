import * as Types from '../generated/graphql';

import { gql } from '@apollo/client';
import * as Apollo from '@apollo/client';
export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
const defaultOptions = {} as const;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
};

export type Collection = {
  __typename?: 'Collection';
  id: Scalars['Int'];
  name: Scalars['String'];
  tasks: Array<Task>;
};

export type LoginResponse = {
  __typename?: 'LoginResponse';
  accessToken?: Maybe<Scalars['String']>;
  ok: Scalars['Boolean'];
};

export type Mutation = {
  __typename?: 'Mutation';
  createCollection: Collection;
  createTag: Tag;
  createTask: Task;
  login: LoginResponse;
  logout: OkResponse;
  register: LoginResponse;
  updateTaskCompletion: UpdateTaskCompletionResponse;
  updateTaskName: UpdateTaskNameResponse;
};


export type MutationCreateCollectionArgs = {
  name: Scalars['String'];
};


export type MutationCreateTagArgs = {
  name: Scalars['String'];
  taskId: Scalars['Int'];
};


export type MutationCreateTaskArgs = {
  collectionId: Scalars['Int'];
  name: Scalars['String'];
};


export type MutationLoginArgs = {
  name: Scalars['String'];
  password: Scalars['String'];
};


export type MutationRegisterArgs = {
  name: Scalars['String'];
  password: Scalars['String'];
};


export type MutationUpdateTaskCompletionArgs = {
  completed: Scalars['Boolean'];
  taskId: Scalars['Int'];
};


export type MutationUpdateTaskNameArgs = {
  taskId: Scalars['Int'];
  taskName: Scalars['String'];
};

export type OkResponse = {
  __typename?: 'OkResponse';
  ok: Scalars['Boolean'];
};

export type Query = {
  __typename?: 'Query';
  collection?: Maybe<Collection>;
  collections: Array<Collection>;
  task?: Maybe<Task>;
};


export type QueryCollectionArgs = {
  id: Scalars['Int'];
};


export type QueryTaskArgs = {
  id: Scalars['Int'];
};

export type Tag = {
  __typename?: 'Tag';
  id: Scalars['Int'];
  name: Scalars['String'];
};

export type Task = {
  __typename?: 'Task';
  completed: Scalars['Boolean'];
  id: Scalars['Int'];
  name: Scalars['String'];
  tags: Array<Tag>;
};

export type UpdateTaskCompletionResponse = {
  __typename?: 'UpdateTaskCompletionResponse';
  completed: Scalars['Boolean'];
};

export type UpdateTaskNameResponse = {
  __typename?: 'UpdateTaskNameResponse';
  name: Scalars['String'];
};

export type CreateCollectionMutationVariables = Types.Exact<{
  name: Types.Scalars['String'];
}>;


export type CreateCollectionMutation = { __typename?: 'Mutation', createCollection: { __typename?: 'Collection', id: number, name: string } };


export const CreateCollectionDocument = gql`
    mutation CreateCollection($name: String!) {
  createCollection(name: $name) {
    id
    name
  }
}
    `;
export type CreateCollectionMutationFn = Apollo.MutationFunction<CreateCollectionMutation, CreateCollectionMutationVariables>;

/**
 * __useCreateCollectionMutation__
 *
 * To run a mutation, you first call `useCreateCollectionMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateCollectionMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createCollectionMutation, { data, loading, error }] = useCreateCollectionMutation({
 *   variables: {
 *      name: // value for 'name'
 *   },
 * });
 */
export function useCreateCollectionMutation(baseOptions?: Apollo.MutationHookOptions<CreateCollectionMutation, CreateCollectionMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<CreateCollectionMutation, CreateCollectionMutationVariables>(CreateCollectionDocument, options);
      }
export type CreateCollectionMutationHookResult = ReturnType<typeof useCreateCollectionMutation>;
export type CreateCollectionMutationResult = Apollo.MutationResult<CreateCollectionMutation>;
export type CreateCollectionMutationOptions = Apollo.BaseMutationOptions<CreateCollectionMutation, CreateCollectionMutationVariables>;