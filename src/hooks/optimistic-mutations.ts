import produce from 'immer';
import { useCreateCollectionMutation } from '../graphql/createCollection.generated';
import { useCreateTagMutation } from '../graphql/createTag.generated';
import { useCreateTaskMutation } from '../graphql/createTask.generated';
import { AllCollectionsDocument } from '../graphql/getCollections.generated';
import { Collection } from '../graphql/register.generated';
import { useUpdateTaskCompletionMutation } from '../graphql/updateTaskCompletion.generated';
import { useUpdateTaskNameMutation } from '../graphql/updateTaskName.generated';

export const useOptimisticUpdateTaskNameMutation = ({
  collectionId,
  taskId,
}: {
  collectionId: number;
  taskId: number;
}) =>
  useUpdateTaskNameMutation({
    update(cache, { data }) {
      const { collections } = cache.readQuery({
        query: AllCollectionsDocument,
      }) as { collections: Collection[] };
      const newCollections = produce(collections, (draft) => {
        for (const collection of draft) {
          if (collection.id === collectionId) {
            for (const task of collection.tasks) {
              if (task.id === taskId) {
                task.name = data!.updateTaskName.name;
              }
            }
          }
        }
      });
      cache.writeQuery({
        query: AllCollectionsDocument,
        data: { collections: newCollections },
      });
    },
  });

export const useOptimisticCreateTagMutation = ({ collectionId, taskId }: { collectionId: number; taskId: number }) =>
  useCreateTagMutation({
    update(cache, { data }) {
      const { collections } = cache.readQuery({
        query: AllCollectionsDocument,
      }) as { collections: Collection[] };
      const newCollections = produce(collections, (draft) => {
        for (const collection of draft) {
          if (collection.id === collectionId) {
            for (const task of collection.tasks) {
              if (task.id === taskId) {
                task.tags.push(data!.createTag);
              }
            }
          }
        }
      });
      cache.writeQuery({
        query: AllCollectionsDocument,
        data: { collections: newCollections },
      });
    },
  });

export const useOptimisticUpdateTaskCompletionMutation = ({
  collectionId,
  taskId,
}: {
  collectionId: number;
  taskId: number;
}) =>
  useUpdateTaskCompletionMutation({
    update(cache, { data }) {
      const { collections } = cache.readQuery({
        query: AllCollectionsDocument,
      }) as { collections: Collection[] };
      const newCollections = produce(collections, (draft) => {
        for (const collection of draft) {
          if (collection.id === collectionId) {
            for (const task of collection.tasks) {
              if (task.id === taskId) {
                task.completed = data!.updateTaskCompletion.completed;
              }
            }
          }
        }
      });
      cache.writeQuery({
        query: AllCollectionsDocument,
        data: { collections: newCollections },
      });
    },
  });

export const useOptimisticCreateTaskMutation = ({ collectionId }: { collectionId: number }) =>
  useCreateTaskMutation({
    update(cache, { data }) {
      const { collections } = cache.readQuery({
        query: AllCollectionsDocument,
      }) as { collections: Collection[] };
      const newCollections = produce(collections, (draft) => {
        for (const collection of draft) {
          if (collection.id === collectionId) {
            collection.tasks.push({
              ...data!.createTask,
              tags: [],
            });
          }
        }
      });
      cache.writeQuery({
        query: AllCollectionsDocument,
        data: { collections: newCollections },
      });
    },
  });

export const useOptimisticCreateCollectionMutation = () =>
  useCreateCollectionMutation({
    update(cache, { data }) {
      const { collections } = cache.readQuery({
        query: AllCollectionsDocument,
      }) as { collections: Collection[] };
      const newCollections = [...collections, { ...data?.createCollection, tasks: [] }];
      cache.writeQuery({
        query: AllCollectionsDocument,
        data: { collections: newCollections },
      });
    },
  });
