import React, { useState } from 'react';
import { useForm } from '@mantine/form';
import { useRegisterMutation } from '../graphql/register.generated';
import { useLoginMutation } from '../graphql/login.generated';
import { useNavigate } from 'react-router-dom';
import { upperFirst, useToggle } from '@mantine/hooks';
import {
  TextInput,
  PasswordInput,
  Text,
  Paper,
  Group,
  PaperProps,
  Button,
  Anchor,
  Stack,
  LoadingOverlay,
} from '@mantine/core';
import { setAccessToken } from '../access-token';

const internalErrorMessage = 'Error occured while calling server, contact support';

export const AuthPage = (props: PaperProps) => {
  const [type, toggle] = useToggle(['login', 'register']);
  const [register] = useRegisterMutation({
    fetchPolicy: 'no-cache',
  });
  const [login] = useLoginMutation({
    fetchPolicy: 'no-cache',
  });
  const [errorTextState, setErrorTextState] = useState<string>('');
  const [loading, setLoading] = useState<boolean>(false);
  const wrappedToggle = () => {
    toggle();
    setErrorTextState('');
  };
  const navigate = useNavigate();
  const mutation = type === 'register' ? register : login;
  const errorMessage =
    type === 'register' ? 'Username already taken, choose a different one' : 'Bad username and/or password';
  const form = useForm({
    initialValues: {
      username: '',
      password: '',
    },
  });

  return (
    <div style={{ position: 'relative', maxWidth: '500px', margin: 'auto' }}>
      <Paper radius="md" p="xl" withBorder {...props}>
        <LoadingOverlay visible={loading} />
        <Text size="lg" weight={700}>
          {type}
        </Text>

        <form onSubmit={form.onSubmit(() => {})}>
          <Stack>
            <TextInput
              required
              label="Username"
              placeholder="Username"
              value={form.values.username}
              onChange={(event) => form.setFieldValue('username', event.currentTarget.value)}
            />

            <PasswordInput
              required
              label="Password"
              placeholder="Your password"
              value={form.values.password}
              onChange={(event) => form.setFieldValue('password', event.currentTarget.value)}
            />
          </Stack>

          <Group position="apart" mt="xl">
            <Anchor component="button" type="button" color="dimmed" onClick={() => wrappedToggle()} size="xs">
              {type === 'register' ? 'Already have an account? Login' : "Don't have an account? Register"}
            </Anchor>
            <Button
              type="submit"
              onClick={async () => {
                setErrorTextState('');
                setLoading(true);
                try {
                  const mutationResult = await mutation({
                    variables: {
                      name: form.getInputProps('username').value,
                      password: form.getInputProps('password').value,
                    },
                    errorPolicy: 'ignore',
                  });
                  if (!mutationResult.data) {
                    setErrorTextState(internalErrorMessage);
                    return;
                  }
                  const { accessToken, ok } =
                    'register' in mutationResult.data ? mutationResult.data.register : mutationResult.data.login;
                  if (!ok || !accessToken) {
                    setErrorTextState(errorMessage);
                    return;
                  }
                  setAccessToken(accessToken);
                  navigate('/');
                } catch (e) {
                  setErrorTextState(internalErrorMessage);
                } finally {
                  setLoading(false);
                }
              }}
            >
              {upperFirst(type)}
            </Button>
          </Group>
        </form>
        {errorTextState && <Text color={'red'}>{errorTextState}</Text>}
      </Paper>
    </div>
  );
};
