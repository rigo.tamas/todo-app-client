import React, { useState } from 'react';
import { Loader, Text, TextInput } from '@mantine/core';
import { useNavigate } from 'react-router-dom';
import { CollectionComponent } from '../components/Collection';
import { InputWithButton } from '../components/Input-with-button';
import { Logout } from '../components/Logout';
import { IconPlus } from '@tabler/icons';
import produce from 'immer';
import { useAllCollectionsQuery } from '../graphql/getCollections.generated';
import { Task } from '../graphql/register.generated';
import { useOptimisticCreateCollectionMutation } from '../hooks/optimistic-mutations';

export const Todos = () => {
  const { data, error } = useAllCollectionsQuery({
    notifyOnNetworkStatusChange: true,
  });
  const [searchText, seatSearchText] = useState<string>('');
  const navigate = useNavigate();
  const [createCollection, { error: errorMutation }] = useOptimisticCreateCollectionMutation();
  const createCollectionWrapped = async (name: string) => {
    createCollection({
      variables: {
        name,
      },
      optimisticResponse: {
        createCollection: {
          id: 0,
          __typename: 'Collection',
          name,
        },
      },
    });
  };
  if (error) {
    navigate('/auth');
  }
  if (!data) {
    return <Loader />;
  }
  const collectionsFiltered = produce(data.collections, (draft) => {
    for (const collection of data.collections) {
      const newTasks: Task[] = [];
      for (const task of collection.tasks) {
        const atLeastOneTagMatch = task.tags.some((tag) => tag.name.includes(searchText));
        if (!searchText || task.name.includes(searchText) || atLeastOneTagMatch) {
          newTasks.push(task);
        }
      }
      const index = draft.findIndex((coll) => coll.id === collection.id);
      if (!newTasks.length && searchText) {
        if (index !== -1) {
          draft.splice(index, 1); // hide collection if no task can be shown for it
        }
      } else {
        draft[index].tasks = newTasks;
      }
    }
  });
  return (
    <>
      <div style={{ position: 'relative', margin: 'auto' }}>
        <Logout></Logout>

        <TextInput
          label="Search"
          radius="xl"
          size="md"
          value={searchText}
          my="lg"
          onChange={(event) => seatSearchText(event.currentTarget.value)}
          placeholder={'Search by name of task or tag name'}
          rightSectionWidth={42}
        />
        <InputWithButton
          label={'Create Collection'}
          Icon={IconPlus}
          onClick={createCollectionWrapped}
          placeholder={'Collection Name'}
        ></InputWithButton>
        {errorMutation && <Text color={'red'}>Error occured while creating collection. Try again.</Text>}
        <ul>
          {collectionsFiltered.map((collection) => (
            <li key={collection.id}>
              <CollectionComponent
                key={collection.id}
                name={collection.name}
                tasks={collection.tasks}
                collectionId={collection.id}
              />
            </li>
          ))}
        </ul>
      </div>
    </>
  );
};
