/* eslint-disable */
import * as types from './graphql';
import { TypedDocumentNode as DocumentNode } from '@graphql-typed-document-node/core';

const documents = {
    "mutation Login($name: String!, $password: String!) {\n  login(name: $name, password: $password) {\n    accessToken\n  }\n}": types.LoginDocument,
};

export function graphql(source: "mutation Login($name: String!, $password: String!) {\n  login(name: $name, password: $password) {\n    accessToken\n  }\n}"): (typeof documents)["mutation Login($name: String!, $password: String!) {\n  login(name: $name, password: $password) {\n    accessToken\n  }\n}"];

export function graphql(source: string): unknown;
export function graphql(source: string) {
  return (documents as any)[source] ?? {};
}

export type DocumentType<TDocumentNode extends DocumentNode<any, any>> = TDocumentNode extends DocumentNode<  infer TType,  any>  ? TType  : never;