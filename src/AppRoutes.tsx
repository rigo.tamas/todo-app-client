import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { AuthPage } from './pages/Auth';
import { Todos } from './pages/Todos';

export function AppRoutes() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Todos />} />
        <Route path="/auth" element={<AuthPage />} />
      </Routes>
    </BrowserRouter>
  );
}
