import { TextInput, TextInputProps, ActionIcon, useMantineTheme } from '@mantine/core';
import { TablerIcon } from '@tabler/icons';
import { useState } from 'react';

export function InputWithButton({
  placeholder,
  onClick,
  label,
  Icon,
  disabled = false,
  defaultValue = '',
}: {
  placeholder: string;
  onClick: (value: string) => void;
  label?: string;
  disabled?: boolean;
  defaultValue?: string;
  Icon: TablerIcon;
}) {
  const [name, setName] = useState<string>(defaultValue);
  return (
    <TextInput
      m={'md'}
      label={label}
      radius="xl"
      size="md"
      disabled={disabled}
      value={name}
      rightSection={
        <ActionIcon
          size={32}
          radius="xl"
          variant="filled"
          onClick={() => {
            onClick(name);
            setName(defaultValue);
          }}
        >
          <Icon />
        </ActionIcon>
      }
      onChange={(event) => setName(event.currentTarget.value)}
      placeholder={placeholder}
      rightSectionWidth={42}
    />
  );
}
