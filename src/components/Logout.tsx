import { useApolloClient } from '@apollo/client';
import { Button } from '@mantine/core';
import React from 'react';
import { useNavigate } from 'react-router-dom';
import { setAccessToken } from '../access-token';
import { useLogoutMutation } from '../graphql/logout.generated';

export const Logout = () => {
  const navigate = useNavigate();
  const [logout] = useLogoutMutation();
  const client = useApolloClient();
  const logoutWrapped = async () => {
    await logout();
    await client.clearStore();
    setAccessToken('');
    navigate('/auth');
  };
  return (
    <Button my={'md'} onClick={() => logoutWrapped()}>
      Logout
    </Button>
  );
};
