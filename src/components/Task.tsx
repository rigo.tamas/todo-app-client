import React, { useRef, useState } from 'react';
import { ActionIcon, Button, Group, Switch, Text, TextInput } from '@mantine/core';
import { Tag } from '../graphql/register.generated';
import { InputWithButton } from './Input-with-button';
import { IconPlus, IconEdit } from '@tabler/icons';
import {
  useOptimisticCreateTagMutation,
  useOptimisticUpdateTaskCompletionMutation,
  useOptimisticUpdateTaskNameMutation,
} from '../hooks/optimistic-mutations';

type Props = {
  name: string;
  tags: Tag[];
  taskId: number;
  completed: boolean;
  collectionId: number;
};

export const TaskComponent = ({ name, tags, taskId, completed, collectionId }: Props) => {
  const [completedState, setCompleted] = useState<boolean>(completed);
  const [isDisabled, setDisabled] = useState<boolean>(true);
  const editTaskNameRef = useRef() as React.MutableRefObject<HTMLInputElement>;
  const [updateTaskName] = useOptimisticUpdateTaskNameMutation({ collectionId, taskId });
  const [createTag] = useOptimisticCreateTagMutation({ collectionId, taskId });
  const [updateCompleted] = useOptimisticUpdateTaskCompletionMutation({ collectionId, taskId });
  const createTagWrapped = async (name: string) => {
    createTag({
      variables: {
        name,
        taskId,
      },
      optimisticResponse: {
        createTag: {
          id: 0,
          name,
        },
      },
    });
  };
  const updateCompletedWrapped = () => {
    updateCompleted({
      variables: {
        completed: !completedState,
        taskId,
      },
    });
    setCompleted(!completedState);
  };
  const updateTaskNameWrapped = (taskName: string) => {
    updateTaskName({
      variables: {
        taskId,
        taskName,
      },
    });
    setDisabled(true);
  };
  return (
    <>
      <Group>
        <TextInput
          m={'md'}
          label={'Edit task name'}
          radius="xl"
          size="md"
          ref={editTaskNameRef}
          disabled={isDisabled}
          defaultValue={name}
          rightSection={
            <ActionIcon
              size={32}
              radius="xl"
              variant="filled"
              onClick={() => {
                setDisabled(!isDisabled);
              }}
            >
              <IconEdit />
            </ActionIcon>
          }
          placeholder={'edit task name'}
          rightSectionWidth={42}
        />
        <Button disabled={isDisabled} onClick={() => updateTaskNameWrapped(editTaskNameRef.current.value)}>
          Update Task name
        </Button>
        <Switch label={'completed'} onChange={updateCompletedWrapped} checked={completedState} />
      </Group>
      <div style={{ maxWidth: '300px' }}>
        <InputWithButton Icon={IconPlus} onClick={createTagWrapped} placeholder={'tag name'} />
      </div>
      {tags.length > 0 && <Text>Tags: {tags.map((tag) => tag.name).join(', ')}</Text>}
    </>
  );
};
