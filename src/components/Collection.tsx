import React from 'react';
import { Group, Paper, Text } from '@mantine/core';
import { Task } from '../graphql/getCollections.generated';
import { TaskComponent } from './Task';
import { IconPlus } from '@tabler/icons';
import { InputWithButton } from './Input-with-button';
import { useOptimisticCreateTaskMutation } from '../hooks/optimistic-mutations';

type Props = {
  name: string;
  tasks: Task[];
  collectionId: number;
};

const sortFunction = (task1: Task, task2: Task): number => {
  if (task1.id === -1) {
    // so that optimistic updates stay on top
    return -1;
  }
  if (task1.id < task2.id) {
    return 1;
  }
  if (task1.id > task2.id) {
    return -1;
  }
  return 0;
};

export const CollectionComponent = ({ name, tasks, collectionId }: Props) => {
  const [createTask, { error: errorMutation }] = useOptimisticCreateTaskMutation({ collectionId });

  const createTaskWrapped = (name: string) => {
    createTask({
      variables: {
        collectionId,
        name,
      },
      optimisticResponse: {
        createTask: {
          completed: false,
          id: -1,
          name,
        },
      },
    });
  };
  return (
    <div style={{ maxWidth: '800px', position: 'relative' }}>
      <Text size="lg" weight={700}>
        {name}
      </Text>
      <Paper radius="md" p="xl" withBorder>
        <Group align="flex-end">
          <InputWithButton Icon={IconPlus} placeholder="Task name" onClick={createTaskWrapped}></InputWithButton>
        </Group>
        {errorMutation && <Text color={'red'}>Error occured while creating Task. Try again.</Text>}
        <Text size="lg" weight={600}>
          Tasks:
        </Text>
        {tasks.length > 0 && (
          <ul>
            {[...tasks].sort(sortFunction).map((task) => (
              <li key={task.id}>
                <Paper my={'sm'} radius="md" p="xl" withBorder>
                  <TaskComponent
                    name={task.name}
                    key={task.id}
                    tags={task.tags}
                    taskId={task.id}
                    collectionId={collectionId}
                    completed={task.completed}
                  ></TaskComponent>
                </Paper>
              </li>
            ))}
          </ul>
        )}
      </Paper>
    </div>
  );
};
