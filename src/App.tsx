import { Loader } from '@mantine/core';
import { useEffect, useState } from 'react';
import { setAccessToken } from './access-token';
import { AppRoutes } from './AppRoutes';

export function App() {
  const [isLoading, setLoading] = useState<boolean>(true);
  useEffect(() => {
    const asyncEffect = async () => {
      setLoading(true);
      try {
        const responseObj = await fetch(`${process.env.REACT_APP_BACKEND_URL}/refresh_token`, {
          method: 'POST',
          credentials: 'include',
        });
        const { accessToken } = (await responseObj.json()) as { accessToken?: string };
        if (!accessToken) {
          return;
        }
        setAccessToken(accessToken);
      } catch (e) {
      } finally {
        setLoading(false);
      }
    };
    void asyncEffect();
  }, []);
  if (isLoading) {
    return <Loader />;
  }
  return <AppRoutes />;
}
