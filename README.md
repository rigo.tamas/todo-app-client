# Simple Todo application -- client

## Local development

Running the app locally can be done as follows:

1. Install dependencies with `npm i`
2. Run the client in watch mode using `npm run start`. The client is reachable at `localhost:3000`

In order for the app to work as intended, be sure to start the server as well.

If there is a change in the GraphQL schema you can update the generated TypeScript types and the apollo hooks with `npm run codegen`

## Building

Building the server can be done with the `npm run build` command. The built app
will be inside the `build/` folder. The app is now deployable
