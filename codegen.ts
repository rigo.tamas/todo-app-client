import type { CodegenConfig } from '@graphql-codegen/cli';

const config: CodegenConfig = {
  overwrite: true,
  schema: `${process.env.REACT_APP_BACKEND_URL}/graphql`,
  documents: 'src/graphql/*.graphql',
  generates: {
    'src/generated/': {
      preset: 'near-operation-file',
      presetConfig: {
        extension: '.generated.tsx',
        baseTypesPath: 'graphql.ts',
      },
      plugins: ['typescript', 'typescript-operations', 'typescript-react-apollo'],
      config: {
        fetcher: {
          endpoint: `${process.env.REACT_APP_BACKEND_URL}/graphql`,
        },
      },
    },
  },
};

export default config;
